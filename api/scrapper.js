var express = require('express');
var fs = require('fs');
var request = require('request');
var cheerio = require('cheerio');
var argv = require('named-argv');
var app = express();

if (argv.opts.url == null) {
  console.log('Please pass --url option');
} else {
  startScrapping(argv.opts.url);
}

// Just for demo, store the found links in memory during runtime and get max 500 links
var foundLinks = [];

function startScrapping(url) {
  console.log('Scrapping: ' + url);
  request(url, function (error, response, html) {
    if (!error) {
      var $ = cheerio.load(html);
      $('a[href^="http://"], a[href^="https://"]').filter(function () {
        var url = $(this).attr('href');
        // Max scrapp 500 links
        if (foundLinks.length < 500) {
          foundLinks.push(url);
          startScrapping(url);
        }
      });
    }
  });
}

app.get('/links', function (req, res) {
  res.setHeader('Content-Type', 'application/json');
  res.send(JSON.stringify(foundLinks));
});

app.listen('8082');

exports = module.exports = app;

