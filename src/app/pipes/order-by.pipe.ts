import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'orderBy'
})
export class OrderByPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    let sortBy = args[0]; // asc or desc

    const sortedArray = [...value]
      .sort((a, b) => {
        let cleanedLinkPrev = a.replace(/^(?:https?:\/\/)?(?:www\.)?/i, "").split('/')[0];
        let cleanedLinkNext = b.replace(/^(?:https?:\/\/)?(?:www\.)?/i, "").split('/')[0];

        if (cleanedLinkPrev.toLowerCase() < cleanedLinkNext.toLowerCase()) //sort string ascending
          return (sortBy === 'asc') ? -1 : 1;
        if (cleanedLinkPrev.toLowerCase() > cleanedLinkNext.toLowerCase())
          return (sortBy === 'asc') ? 1 : -1;
        return 0; //default return value (no sorting)
      });

    return sortedArray;
  }

}
