import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {LinksComponent} from './components/links/links.component';
import {HttpClientModule} from "@angular/common/http";
import {MatButtonModule, MatIconModule, MatListModule, MatToolbarModule} from "@angular/material";
import {PageWrapperComponent} from './components/page-wrapper/page-wrapper.component';
import {OrderByPipe} from './pipes/order-by.pipe';

@NgModule({
  declarations: [
    AppComponent,
    LinksComponent,
    PageWrapperComponent,
    OrderByPipe
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    MatToolbarModule,
    MatListModule,
    MatButtonModule,
    MatIconModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
