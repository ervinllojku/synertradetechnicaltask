import {Component, OnInit} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import "rxjs/add/operator/map";

@Component({
  selector: 'app-links',
  templateUrl: './links.component.html',
  styleUrls: ['./links.component.css']
})
export class LinksComponent implements OnInit {

  sortBy = 'asc';

  public links: string[] = [];

  constructor(private http: HttpClient) {
  }

  ngOnInit() {
    this.fetchLinks();
  }

  private fetchLinks() {
    this.http.get('http://localhost:8082/links')
      .subscribe((response: string[]) => {
        this.links = response;
      });
  }

}
