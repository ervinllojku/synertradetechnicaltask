Steps to run the project:

1. Install all dependencies `npm install` or `yarn`
2. Run js web scrapper `node api/scrapper.js --url=https://en.wikipedia.org/wiki/Europe`
3. Run UI `ng serve`
